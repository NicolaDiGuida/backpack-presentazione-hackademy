<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Dropbox as dropbox;

class UploadToDropbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:dropbox';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
            {   
                
                $directories = scandir(storage_path('backups'), SCANDIR_SORT_DESCENDING);

                $backupDir = storage_path('backups').DIRECTORY_SEPARATOR.$directories[0];
                // $files = scandir($backupDir, SCANDIR_SORT_DESCENDING);
                $files = glob($backupDir.DIRECTORY_SEPARATOR.'*.zip');
                usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
                $backupName = basename($files[0]);
                $backup = $backupDir.DIRECTORY_SEPARATOR.$backupName;
                $this->dropboxUpload($backup,$backupName);
                //eliminazione dei vecchi backup sul server
                for ($i=1; $i <count($files) ; $i++) { 
                    if( unlink( $backupDir.DIRECTORY_SEPARATOR.basename( $files[$i] ) ) )
                        print_r("eliminato backup:".basename( $files[$i] )."\n");
                    else
                        print_r( "impossibile eliminare backup:".basename( $files[$i] )."\n" );
                }
            }


     function getWebAuth()
            {
                $json= [];
                $json['key'] =  env('DROPBOX_KEY');
                $json['secret'] = env('DROPBOX_SECRET');
                $appInfo = new dropbox\AppInfo($json['key'], $json['secret'] );
                $clientIdentifier = "hackademy";
                $redirectUri = "http://localhost:8000/dropbox-auth-finish";//(la rotta non deve esistere per forza)
                $csrfTokenStore = new dropbox\ArrayEntryStore($_SESSION, 'dropbox-auth-csrf-token');
                return new dropbox\WebAuth($appInfo, $clientIdentifier, $redirectUri, $csrfTokenStore);
            }

    public function dropboxUpload($filepath,$filename)
            {
                $webAuth = $this->getWebAuth();
                $accessToken = env('DROPBOX_TOKEN');
                $clientIdentifier = $webAuth->getClientIdentifier();
                $client = new dropbox\Client($accessToken,$clientIdentifier);
                Dropbox::createFolder('/backup');
                $fd = fopen($filepath, "rb");
                $result = $client->uploadFile('/backup/'.$filename, dropbox\WriteMode::force(),$fd);
                fclose($fd);
                print_r($result);  
            }
}
